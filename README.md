# RORO2
This is an implementation of algorithm 2 of the master thesis work 
"Automation of vehicles placement planning in RO-PAX ship loading"

Before running the application (RORO2.jar), please make sure Java SE 12.0.2 is installed and the converter conv.exe is located in one folder
with RORO2.jar file and empty subfolder "Tallink" also exists. These files can be found in the binaries link on page 73 in the thesis.


Template file extension: TXT 
Template file structure:

TYPE_OF_VEHICLE NUMBER_OF_VEHICLES_TO_LOAD 

Example of template file:
MC 0
CAR 221
SHP_CAR 4
VAN 15
L_L 1
BUS 2
LOR 10
TT_25 20
TT_39 2
TLNK 1 
*************************
NOTE: ALLOWED TYPE_OF_VEHICLE IN FILE: 
MC - motorcycle
CAR - car
SHP_CAR - shopping car
VAN - van 
VAN_H - van high
L_L - long and low vehicle
L_MH - long and medium high vehicle
BUS - bus
LOR - lorry
TT_25 - truck or trailer 25 tons
TT_39  - truck or trailer 39 tons
MCH - machine
TRL - trailer 
TLNK - TALLINK trailer