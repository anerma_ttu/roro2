public enum LaneCoefficient {
    P71 (1),
    P72 (Math.pow(10,2)),
    P73 (Math.pow(10,4)),
    P74 (Math.pow(10,6)),
    P61 (Math.pow(10,8)),
    P62 (Math.pow(10,10)),
    P63 (Math.pow(10,12)),
    P64 (Math.pow(10,14)),
    P51 (Math.pow(10,16)),
    P52 (Math.pow(10,18)),
    P53 (Math.pow(10,20)),
    P54 (Math.pow(10,22)),
    P55 (Math.pow(10,24)),
    P31 (Math.pow(10,26)),
    P32 (Math.pow(10,28)),
    P33 (Math.pow(10,30)),
    P34 (Math.pow(10,32)),
    P35 (Math.pow(10,34));

    private double laneCoefficient;

    LaneCoefficient(double laneCoefficient)
    {
        this.laneCoefficient=laneCoefficient;
    }

    double getLaneCoefficient ()
    {
        return laneCoefficient;
    }
}

