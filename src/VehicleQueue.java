import com.sun.tools.javac.Main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class VehicleQueue implements ActionListener {

    // NUMBERS INTEGERS
    private static int totalCars;
    private static int numMC;
    private static int numCAR;
    private static int numSHP_CAR;
    private static int numVAN;
    private static int numVAN_H;
    private static int numL_L;
    private static int numL_MH;
    private static int numBUS;
    private static int numLOR;
    private static int numTT_25;
    private static int numTT_39;
    private static int numMCH;
    private static int numTRL;
    private static int numTLNK;

    // COLLECTION
    private Queue<Vehicle> fullQueue=new LinkedList<>();
    private int total;
    public static float totalCargo;
    public static int totalLoaded;
    public static int totalUnloaded;
    public static float totalUnloadedMass;
    public static int totalLeft;
    private Ship thisShip;
    boolean loadError= false;


    public VehicleQueue(Ship ship) {
        thisShip =ship;

    }

   
    public static int getNumMC() {
        return numMC;
    }

    public static void setNumMC(int numMC) { VehicleQueue.numMC = numMC; }


    public static int getNumCAR() {
        return numCAR;
    }

    public static void setNumCAR(int numCAR) {
        VehicleQueue.numCAR = numCAR;
    }

    public static int getNumSHP_CAR() {
        return numSHP_CAR;
    }

    public static void setNumSHP_CAR(int numSHP_CAR) {
        VehicleQueue.numSHP_CAR = numSHP_CAR;
    }

    public static int getNumVAN() {
        return numVAN;
    }

    public static void setNumVAN(int numVAN) {
        VehicleQueue.numVAN = numVAN;
    }

    public static int getNumVAN_H() {
        return numVAN_H;
    }

    public static void setNumVAN_H(int numVAN_H) {
        VehicleQueue.numVAN_H = numVAN_H;
    }

    public static int getNumL_L() {
        return numL_L;
    }

    public static void setNumL_L(int numL_L) {
        VehicleQueue.numL_L = numL_L;
    }

    public static int getNumL_MH() {
        return numL_MH;
    }

    public static void setNumL_MH(int numL_MH) {
        VehicleQueue.numL_MH = numL_MH;
    }

    public static int getNumBUS() {
        return numBUS;
    }

    public static void setNumBUS(int numBUS) {
        VehicleQueue.numBUS = numBUS;
    }

    public static int getNumLOR() {
        return numLOR;
    }

    public static void setNumLOR(int numLOR) {
        VehicleQueue.numLOR = numLOR;
    }

    public static int getNumTRL() {
        return numTRL;
    }

    public static void setNumTRL(int numTRL) {
        VehicleQueue.numTRL = numTRL;
    }

    public static int getNumTLNK() {
        return numTLNK;
    }

    public static void setNumTLNK(int numTLNK) {
        VehicleQueue.numTLNK = numTLNK;
    }

    public static int getNumTT_25() {
        return numTT_25;
    }

    public static void setNumTT_25(int numTT_25) {
        VehicleQueue.numTT_25 = numTT_25;
    }

    public static int getNumTT_39() {
        return numTT_39;
    }

    public static void setNumTT_39(int numTT_39) {
        VehicleQueue.numTT_39 = numTT_39;
    }

    public static int getNumMCH() {
        return numMCH;
    }

    public static void setNumMCH(int numMCH) {
        VehicleQueue.numMCH = numMCH;
    }

    public void resetTotal() {
        total = 0;
    }

    public void setTotal() {
        this.total = InputData.getTotalCars();
    }

    public int getTotal() {
        return total;
    }

    public void emptyQueue() {

    }

    public static double round3( double value ) {
        int iValue = ( int ) ( value * 1000 );
        double dValue = value * 1000;
        if ( dValue - iValue >= 0.5 ) {
            iValue += 1;
        }
        dValue = ( double ) iValue;
        return dValue / 1000;
    }

    public void getNumbers() {
        this.setTotal();
        MainFrame.addData("\nMC :" + VehicleQueue.getNumMC());
        MainFrame.addData("\nCAR :" + VehicleQueue.getNumCAR());
        MainFrame.addData("\nSHP_CAR: " + VehicleQueue.getNumSHP_CAR());
        MainFrame.addData("\nVAN: " + VehicleQueue.getNumVAN());
        MainFrame.addData("\nVAN_H: " + VehicleQueue.getNumVAN_H());
        MainFrame.addData("\nL_L: " + VehicleQueue.getNumL_L());
        MainFrame.addData("\nL_MH :" + VehicleQueue.getNumVAN_H());
        MainFrame.addData("\nBUS: " + VehicleQueue.getNumBUS());
        MainFrame.addData("\nLOR: " + VehicleQueue.getNumLOR());
        MainFrame.addData("\nTT_25: " + VehicleQueue.getNumTT_25());
        MainFrame.addData("\nTT_39: " + VehicleQueue.getNumTT_39());
        MainFrame.addData("\nMCH: " + VehicleQueue.getNumMCH());
        MainFrame.addData("\nTRL: " + VehicleQueue.getNumTRL());
        MainFrame.addData("\nTLNK: " + VehicleQueue.getNumTLNK());
        MainFrame.addData("\nTotal received : " + this.getTotal());
    }

    public void createQueue() {
        MainFrame.addData("\n Creating queue started");
        int totalCreated=0;
        for (int a = 0; a < VehicleQueue.getNumMC(); a++) {
            fullQueue.add(new StandardVehicle(VehicleType.MC, StandardVehicle.registrationNumberGenerator()));
            totalCreated++;
            totalCargo+=0.2f;
        }

        for (int a = 0; a < VehicleQueue.getNumCAR(); a++) {
            fullQueue.add(new StandardVehicle(VehicleType.CAR, StandardVehicle.registrationNumberGenerator()));
            totalCreated++;
            totalCargo+=1.8f;
        }
        for (int a=0; a<VehicleQueue.getNumSHP_CAR();a++){
            fullQueue.add(new StandardVehicle(VehicleType.SHP_CAR, StandardVehicle.registrationNumberGenerator()));
            totalCreated++;
            totalCargo+=1.8f;
        }
        for (int a=0; a<VehicleQueue.getNumVAN();a++){
            fullQueue.add(new StandardVehicle(VehicleType.VAN, StandardVehicle.registrationNumberGenerator()));
            totalCreated++;
            totalCargo+=2.5f;
        }
        for (int a=0; a<VehicleQueue.getNumVAN_H();a++){
            fullQueue.add(new StandardVehicle(VehicleType.VAN_H, StandardVehicle.registrationNumberGenerator()));
            totalCreated++;
            totalCargo+=2.5f;
        }
        for (int a=0; a<VehicleQueue.getNumL_L();a++){
            fullQueue.add(new StandardVehicle(VehicleType.L_L, StandardVehicle.registrationNumberGenerator()));
            totalCreated++;
            totalCargo+=5f;
        }
        for (int a=0; a<VehicleQueue.getNumL_MH();a++){
            fullQueue.add(new StandardVehicle(VehicleType.L_MH, StandardVehicle.registrationNumberGenerator()));
            totalCreated++;
            totalCargo+=5f;
        }
        for (int a=0; a<VehicleQueue.getNumBUS();a++){
            fullQueue.add(new StandardVehicle(VehicleType.BUS, StandardVehicle.registrationNumberGenerator()));
            totalCreated++;
            totalCargo+=12f;
        }
        for (int a=0; a<VehicleQueue.getNumLOR();a++){
            fullQueue.add(new StandardVehicle(VehicleType.LOR, StandardVehicle.registrationNumberGenerator()));
            totalCreated++;
            totalCargo+=12f;
        }
        for (int a=0; a<VehicleQueue.getNumTT_25();a++){
            fullQueue.add(new StandardVehicle(VehicleType.TT_25, StandardVehicle.registrationNumberGenerator()));
            totalCreated++;
            totalCargo+=25f;
        }
        for (int a=0; a<VehicleQueue.getNumTT_39();a++){
            fullQueue.add(new StandardVehicle(VehicleType.TT_39, StandardVehicle.registrationNumberGenerator()));
            totalCreated++;
            totalCargo+=39f;
        }
        for (int a=0; a<VehicleQueue.getNumMCH();a++){
            fullQueue.add(new StandardVehicle(VehicleType.MCH, StandardVehicle.registrationNumberGenerator()));
            totalCreated++;
            totalCargo+=15f;
        }
        for (int a=0; a<VehicleQueue.getNumTRL();a++){
            fullQueue.add(new StandardVehicle(VehicleType.TRL, StandardVehicle.registrationNumberGenerator()));
            totalCreated++;
            totalCargo+=15f;
        }
        for (int a=0; a<VehicleQueue.getNumTLNK();a++){
            fullQueue.add(new StandardVehicle(VehicleType.TLNK, StandardVehicle.registrationNumberGenerator()));
            totalCreated++;
            totalCargo+=1f;
        }


        MainFrame.addData("\n Total cargo: " +totalCargo);
        MainFrame.addData("\n Creating queue finished");
        MainFrame.addData("\n Size of queue: "+fullQueue.size());
    }

    public void createStandardQueue() {
        MainFrame.addData("\n Creating queue started");
        int totalCreated=0;
        for (int a = 0; a < VehicleQueue.getNumBUS(); a++) {
            fullQueue.add(new StandardVehicle(VehicleType.BUS, StandardVehicle.registrationNumberGenerator()));
            totalCreated++;
            totalCargo+=12f;
        }
        for (int a = 0; a < VehicleQueue.getNumTT_39(); a++) {
            fullQueue.add(new StandardVehicle(VehicleType.TT_39, StandardVehicle.registrationNumberGenerator()));
            totalCreated++;
            totalCargo+=39f;
        }
        for (int a=0; a<VehicleQueue.getNumTT_25();a++){
            fullQueue.add(new StandardVehicle(VehicleType.TT_25, StandardVehicle.registrationNumberGenerator()));
            totalCreated++;
            totalCargo+=25f;
        }
        for (int a=0; a<VehicleQueue.getNumTLNK();a++){
            fullQueue.add(new StandardVehicle(VehicleType.TLNK, StandardVehicle.registrationNumberGenerator()));
            totalCreated++;
            totalCargo+=15f;
        }
        for (int a=0; a<VehicleQueue.getNumTRL();a++){
            fullQueue.add(new StandardVehicle(VehicleType.TRL, StandardVehicle.registrationNumberGenerator()));
            totalCreated++;
            totalCargo+=15f;
        }
        for (int a=0; a<VehicleQueue.getNumMCH();a++){
            fullQueue.add(new StandardVehicle(VehicleType.MCH, StandardVehicle.registrationNumberGenerator()));
            totalCreated++;
            totalCargo+=15f;
        }
        for (int a=0; a<VehicleQueue.getNumLOR();a++){
            fullQueue.add(new StandardVehicle(VehicleType.LOR, StandardVehicle.registrationNumberGenerator()));
            totalCreated++;
            totalCargo+=12f;
        }
        for (int a=0; a<VehicleQueue.getNumL_MH();a++){
            fullQueue.add(new StandardVehicle(VehicleType.L_MH, StandardVehicle.registrationNumberGenerator()));
            totalCreated++;
            totalCargo+=5f;
        }

        for (int a=0; a<VehicleQueue.getNumL_L();a++){
            fullQueue.add(new StandardVehicle(VehicleType.L_L, StandardVehicle.registrationNumberGenerator()));
            totalCreated++;
            totalCargo+=5f;
        }
        for (int a=0; a<VehicleQueue.getNumVAN_H();a++){
            fullQueue.add(new StandardVehicle(VehicleType.VAN_H, StandardVehicle.registrationNumberGenerator()));
            totalCreated++;
            totalCargo+=2.5f;
        }

        for (int a=0; a<VehicleQueue.getNumVAN();a++){
            fullQueue.add(new StandardVehicle(VehicleType.VAN, StandardVehicle.registrationNumberGenerator()));
            totalCreated++;
            totalCargo+=2.5f;
        }

        for (int a=0; a<VehicleQueue.getNumSHP_CAR();a++){
            fullQueue.add(new StandardVehicle(VehicleType.SHP_CAR, StandardVehicle.registrationNumberGenerator()));
            totalCreated++;
            totalCargo+=1.8f;
        }

        for (int a=0; a<VehicleQueue.getNumCAR();a++){
            fullQueue.add(new StandardVehicle(VehicleType.CAR, StandardVehicle.registrationNumberGenerator()));
            totalCreated++;
            totalCargo+=1.8f;
        }
        for (int a=0; a<VehicleQueue.getNumMC();a++){
            fullQueue.add(new StandardVehicle(VehicleType.MC, StandardVehicle.registrationNumberGenerator()));
            totalCreated++;
            totalCargo+=0.2f;
        }
        MainFrame.addData("\n Total cargo: " + totalCargo);
        MainFrame.addData("\n Creating queue finished");
        MainFrame.addData("\n Size of queue: "+fullQueue.size());
        totalLeft=totalCreated;

    }

    public void inspectQueue() {
        for (Vehicle item : fullQueue)
        {
            item.getVehicleData();
        }

    }

    public Vehicle popFromQueue ()
    {
        Vehicle temp = fullQueue.peek();
        return temp;
    }

    public void populateTypeList(List<VehicleType> loadVehicleTypeList)
    {
        loadVehicleTypeList.add(VehicleType.MC);
        loadVehicleTypeList.add(VehicleType.CAR);
        loadVehicleTypeList.add(VehicleType.SHP_CAR);
        loadVehicleTypeList.add(VehicleType.VAN);
        loadVehicleTypeList.add(VehicleType.VAN_H);
        loadVehicleTypeList.add(VehicleType.L_L);
        loadVehicleTypeList.add(VehicleType.L_MH);
        loadVehicleTypeList.add(VehicleType.BUS);
        loadVehicleTypeList.add(VehicleType.LOR);
        loadVehicleTypeList.add(VehicleType.TT_25);
        loadVehicleTypeList.add(VehicleType.TT_39);
        loadVehicleTypeList.add(VehicleType.MCH);
        loadVehicleTypeList.add(VehicleType.TRL);
        loadVehicleTypeList.add(VehicleType.TLNK);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        Ferry.setDirection(MainFrame.getRoute());
        MainFrame.addData("\nDirection: " + Ferry.getDirection());
        for (Lane currentLane: thisShip.getLanes())
        {
            currentLane.calculateInitialSetPoint();
            currentLane.calculateAvailableLength();
        }
        MainFrame.addData("\nStarted creating objects... ");
        this.getNumbers();
        this.createStandardQueue();
        this.inspectQueue();
        while (!this.fullQueue.isEmpty()) {
            try {
                thisShip.calculatePriorityLanes();
                thisShip.markLanes(fullQueue.peek());
                thisShip.sortLanes();
                thisShip.roomCheck();
                if (thisShip.loadVehicle(fullQueue.peek(), thisShip.getBestLane()) == 1 && !thisShip.getError()) {
                    fullQueue.poll();
                    totalLoaded += 1;
                    totalLeft-=1;
                }
                else
                {
                    MainFrame.addData("\nVehicle " + fullQueue.peek().getRegistrationNumber() + " has not been loaded");
                    totalUnloadedMass+=fullQueue.peek().getVehicleMass();
                    MainFrame.addData("\nNo more room on ferry");
                    totalUnloaded +=1;
                    totalLeft-=1;
                    fullQueue.poll();

                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        //TODO
        for (Lane currentLane: thisShip.getLanes()) {
            if (currentLane.getLaneMass() > 0.0f) {
                List<Vehicle> tempList = currentLane.getVehicleList();
                float tempXMIN=0, tempXMAX=0, tempCenterX=0, centerX=0;
                if (Ferry.getDirection()) {
                     tempXMAX = tempList.get(0).getxMax();
                     tempXMIN = tempList.get(tempList.size() - 1).getxMin();

                    tempCenterX = tempXMIN + (float)round3((tempXMAX - tempXMIN) / 2);
                    centerX = currentLane.getCenterX();
                    for (Vehicle currentVehicle : tempList) {
                        currentVehicle.setXmin(currentVehicle.getxMin() + (centerX - tempCenterX));
                        currentVehicle.setXmax(currentVehicle.getxMax() + (centerX - tempCenterX));
                    }

                } else {
                    {
                        tempXMIN = tempList.get(0).getxMin();
                        tempXMAX = tempList.get(tempList.size() - 1).getxMax();
                        tempCenterX = tempXMIN + (float)round3((tempXMAX - tempXMIN) / 2);
                        centerX = currentLane.getCenterX();
                        for (Vehicle currentVehicle : tempList) {
                            currentVehicle.setXmin(currentVehicle.getxMin() + (centerX - tempCenterX));
                            currentVehicle.setXmax(currentVehicle.getxMax() + (centerX - tempCenterX));
                        }

                    }


                }
            }
        }




        thisShip.sortToDefault();
        for (Lane currentlane : thisShip.getLanes ())
        {
            if (currentlane.getLaneMass()> 0.0f)
            {
                currentlane.getLoadedVehiclesOnLane ();
                currentlane.printLaneMass( currentlane);
            }
        }
        thisShip.sortToDefault();

        for (Lane currentLane: thisShip.getLanes())
        {
            if (!(currentLane.getVehicleList().isEmpty()))
                currentLane.exportToTable(currentLane.getVehicleList(), currentLane, thisShip.getExportTable());
        }
        // String expTable = thisShip.getExportTableHeader();
        String expTable ="";
        for (Record currentRecord: thisShip.getExportTable())
        {
                expTable+= currentRecord.toString();
        }

        BigDecimal totalCargoMass = new BigDecimal(0);
        BigDecimal X= new BigDecimal(0);
        BigDecimal Y= new BigDecimal(0);
        BigDecimal Z = new BigDecimal(0);
        for (Record currentRecord: thisShip.getExportTable())
        {
            totalCargoMass=totalCargoMass.add(new BigDecimal(round3(currentRecord.getMASS())));
            X = X.add (new BigDecimal(round3(currentRecord.getXM())).multiply(new BigDecimal(round3(currentRecord.getMASS()))));
            Y = Y.add (new BigDecimal(round3(currentRecord.getYM())).multiply(new BigDecimal(round3(currentRecord.getMASS()))));
            Z = Z.add (new BigDecimal(round3(currentRecord.getZM())).multiply(new BigDecimal(round3(currentRecord.getMASS()))));

        }
        BigDecimal X2=new BigDecimal(String.valueOf(X.divide(totalCargoMass.setScale(5, RoundingMode.HALF_UP),5,RoundingMode.HALF_UP)));
        BigDecimal Y2=new BigDecimal(String.valueOf(Y.divide(totalCargoMass.setScale(5, RoundingMode.HALF_UP),5,RoundingMode.HALF_UP)));
        BigDecimal Z2=new BigDecimal(String.valueOf(Z.divide(totalCargoMass.setScale(5, RoundingMode.DOWN),5,RoundingMode.DOWN)));


        MainFrame.addData("\nTotal vehicle loaded :" + totalLoaded);
        MainFrame.addData("\n Total vehicle unloaded: " + totalUnloaded);
        MainFrame.addData("\n Total unloaded mass: " + round3(totalUnloadedMass));
        MainFrame.addData("\n Total loaded mass :" + totalCargoMass.setScale(3,RoundingMode.DOWN));
        MainFrame.addData("\nGravity point lightweight: X = 88.13, Y = 0.07, Z= 14.06");
        MainFrame.addData("\nGravity point deadweight: X = " + X2 + ", Y= "+ Y2 + ", Z= " +Z2 );
        //MainFrame.addData("\nDeviation: X = "+ Math.abs(88.13-X) + ", Y= "+ Math.abs(0.07-Y) + ", Z= " + Math.abs(14.06-Z));

            /*
         String expLoadTable = thisShip.getExportLoadTableHeader();

          List <VehicleType> loadVehicleTypeList=  new LinkedList<>();
          populateTypeList(loadVehicleTypeList);
          thisShip.exportToLoadTable(loadVehicleTypeList, thisShip.getExportLoadTable() );
        for (LoadRecord currentLoadRecord: thisShip.getExportLoadTable())
        {
            expLoadTable+= currentLoadRecord.toString();
        }
        */
        //MainFrame.addData(expTable);
        String mid1=expTable.replace("L_L", "L+L");
        String fileExp=mid1.replace("L_MH", "L+MH");
        //  MainFrame.addData(expLoadTable);

        File file = new File("Tallink/results.txt");
        //MainFrame.addData(file.getAbsolutePath());
        FileWriter writer = null;
        try {
            writer = new FileWriter(file);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        // Запись содержимого в файл
        try {
            writer.write(fileExp);
            //  writer.write(expLoadTable);
            writer.flush();
            writer.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        try {
            File exeFile = new File("conv.exe");
            System.out.println(exeFile.getAbsolutePath());
            File outFile = new File("Tallink/results.ld");
            outFile.delete();
            outFile.createNewFile();
            //String cmd = "C:\\Windows\\System32\\cmd.exe /c " + "\"" + exeFile.getAbsolutePath() +
             String cmd =  "conv.exe " + " ./Tallink/results.txt" + " ./Tallink/results.ld";
            outFile = null;
            Runtime.getRuntime().exec(cmd);
            System.out.println(cmd);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        MainFrame.deactivateStartProcessButton();

    }




}