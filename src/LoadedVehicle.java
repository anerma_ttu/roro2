public class LoadedVehicle implements Vehicle {

    StandardVehicle loadedVehicle;
    String laneNum;
    String laneDeck;
    float xMin;
    float xMax;

    LoadedVehicle (StandardVehicle loaded, String laneNum, String laneDeck)
    {
        this.loadedVehicle=loaded;
        this.laneNum=laneNum;
        this.laneDeck=laneDeck;
    }

    @Override
    public String getRegistrationNumber() {
        return loadedVehicle.getRegistrationNumber();
    }

    @Override
    public void setRegistrationNumber(String registrationNumber) {
        loadedVehicle.setRegistrationNumber(registrationNumber);
    }

    @Override
    public VehicleType getVehicleType() {
        return loadedVehicle.itsType;
    }

    @Override
    public float getVehicleLength() {
        return loadedVehicle.getVehicleLength();
    }

    @Override
    public float getVehicleWidth() {
        return loadedVehicle.getVehicleWidth();
    }
    @Override
    public float getVehicleHeight() {
        return loadedVehicle.getVehicleHeight();
    }

    @Override
    public void setXmax(float v) {
        xMax=v;
    }

    @Override
    public void setXmin(float v) {
        xMin=v;
    }

    public void setxMin (float number) {xMin=number;}

    public float getxMin () {return xMin;}

    public void setxMax (float number) { xMax=number;}

    public float getxMax () {return xMax;}

    @Override
    public void getVehicleData() {
        MainFrame.addData("\n Loaded vehicle info");
        MainFrame.addData("\n Type: " + this.getVehicleType());
        MainFrame.addData("\n Reg number: " + this.getRegistrationNumber());
        MainFrame.addData("\n Lane:" + laneNum + ", Lane deck: " + laneDeck);
        MainFrame.addData("\n Xmin:" + xMin + ", Xmax: " + xMax +"\n");
    }

    @Override
    public float getVehicleMass() {
        return loadedVehicle.getVehicleMass();
    }

    void setMaxX (float setPoint, float vehicleLength)

    {
        if (Ferry.getDirection())
            xMax=setPoint;
        else
            xMax=setPoint+vehicleLength;
    }
    void setMinX (float setPoint, float vehicleLength)

    {
        if (Ferry.getDirection())
            xMin=setPoint-vehicleLength;
        else
            xMin=setPoint;
    }

    public String getLaneString () { return laneNum;}
    public String getDeckString () { return laneDeck;}


}

