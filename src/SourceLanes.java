import java.util.*;

public class SourceLanes {

    List testLanes = new ArrayList<StandardLane>();

    public SourceLanes() {
        testLanes = new ArrayList<>();
    }

    public void initLanes() {
        {
            testLanes.add(new StandardLane("L11", "3.DECK", 3, 12.95f,
                    9.9f, 15f, 125f, 1,0, LaneCoefficient.P31.getLaneCoefficient(),LaneCoefficient.P31.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.FULL);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.VAN);
                            add(VehicleType.VAN_H);
                            add(VehicleType.L_L);
                            add(VehicleType.L_MH);
                            add(VehicleType.BUS);
                            add(VehicleType.LOR);
                            add(VehicleType.TT_25);
                            add(VehicleType.TT_39);
                            add(VehicleType.MCH);
                            add(VehicleType.TRL);
                            add(VehicleType.TLNK);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                }
            }
            ));
            testLanes.add(new StandardLane("L12", "3.DECK", 3,
                    9.85f, 9.9f, 1f, 149.0f, 2, 0, LaneCoefficient.P32.getLaneCoefficient(),LaneCoefficient.P32.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.FULL);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.VAN);
                            add(VehicleType.VAN_H);
                            add(VehicleType.L_L);
                            add(VehicleType.L_MH);
                            add(VehicleType.BUS);
                            add(VehicleType.LOR);
                            add(VehicleType.TT_25);
                            add(VehicleType.TT_39);
                            add(VehicleType.MCH);
                            add(VehicleType.TRL);
                            add(VehicleType.TLNK);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                }
            }
            ));
            testLanes.add(new StandardLane("L13", "3.DECK", 3,
                    6.75f, 9.9f, -3f, 158f,3, 0, LaneCoefficient.P33.getLaneCoefficient(),LaneCoefficient.P33.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.FULL);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.VAN);
                            add(VehicleType.VAN_H);
                            add(VehicleType.L_L);
                            add(VehicleType.L_MH);
                            add(VehicleType.BUS);
                            add(VehicleType.LOR);
                            add(VehicleType.TT_25);
                            add(VehicleType.TT_39);
                            add(VehicleType.MCH);
                            add(VehicleType.TRL);
                            add(VehicleType.TLNK);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                }
            }
            ));
            testLanes.add(new StandardLane("L14A", "3.DECK", 3,
                    3.65f, 9.9f, -3f, 28f, 4,0, LaneCoefficient.P34.getLaneCoefficient(),LaneCoefficient.P34.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.STERN);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.VAN);
                            add(VehicleType.VAN_H);
                            add(VehicleType.L_L);
                            add(VehicleType.L_MH);
                            add(VehicleType.BUS);
                            add(VehicleType.LOR);
                            add(VehicleType.TT_25);
                            add(VehicleType.TT_39);
                            add(VehicleType.MCH);
                            add(VehicleType.TRL);
                            add(VehicleType.TLNK);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                }
            }
            ));
            testLanes.add(new StandardLane("L14F", "3.DECK", 3,
                    3.65f, 9.9f, 150f, 182f,5, 0, LaneCoefficient.P34.getLaneCoefficient(),LaneCoefficient.P34.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.BOW);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);

                            add(VehicleType.VAN);
                            add(VehicleType.VAN_H);
                            add(VehicleType.L_L);
                            add(VehicleType.L_MH);
                            add(VehicleType.BUS);
                            add(VehicleType.LOR);
                            add(VehicleType.TT_25);
                            add(VehicleType.TT_39);
                            add(VehicleType.MCH);
                            add(VehicleType.TRL);
                            add(VehicleType.TLNK);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                }
            }
            ));
            testLanes.add(new StandardLane("L15", "3.DECK", 3,
                    -0.55f, 9.9f, 0.0f, 158f, 6,0, LaneCoefficient.P35.getLaneCoefficient(),LaneCoefficient.P35.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.FULL);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.VAN);
                            add(VehicleType.VAN_H);
                            add(VehicleType.L_L);
                            add(VehicleType.L_MH);
                            add(VehicleType.BUS);
                            add(VehicleType.LOR);
                            add(VehicleType.TT_25);
                            add(VehicleType.TT_39);
                            add(VehicleType.MCH);
                            add(VehicleType.TRL);
                            add(VehicleType.TLNK);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                }
            }
            ));

            testLanes.add(new StandardLane("L16", "3.DECK", 3,
                    -3.65f, 9.9f, -3f, 182f, 7,0, LaneCoefficient.P34.getLaneCoefficient(),LaneCoefficient.P34.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.FULL);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);

                            add(VehicleType.VAN);
                            add(VehicleType.VAN_H);
                            add(VehicleType.L_L);
                            add(VehicleType.L_MH);
                            add(VehicleType.BUS);
                            add(VehicleType.LOR);
                            add(VehicleType.TT_25);
                            add(VehicleType.TT_39);
                            add(VehicleType.MCH);
                            add(VehicleType.TRL);
                            add(VehicleType.TLNK);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                }
            }
            ));

            testLanes.add(new StandardLane("L17", "3.DECK", 3,
                    -6.75f, 9.9f, -3f, 158f,8, 0, LaneCoefficient.P33.getLaneCoefficient(),LaneCoefficient.P33.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.FULL);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);

                            add(VehicleType.VAN);
                            add(VehicleType.VAN_H);
                            add(VehicleType.L_L);
                            add(VehicleType.L_MH);
                            add(VehicleType.BUS);
                            add(VehicleType.LOR);
                            add(VehicleType.TT_25);
                            add(VehicleType.TT_39);
                            add(VehicleType.MCH);
                            add(VehicleType.TRL);
                            add(VehicleType.TLNK);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                }
            }
            ));

            testLanes.add(new StandardLane("L18", "3.DECK", 3,
                    -9.85f, 9.9f, 1f, 149f, 9,0, LaneCoefficient.P32.getLaneCoefficient(),LaneCoefficient.P32.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.FULL);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.VAN);
                            add(VehicleType.VAN_H);
                            add(VehicleType.L_L);
                            add(VehicleType.L_MH);
                            add(VehicleType.BUS);
                            add(VehicleType.LOR);
                            add(VehicleType.TT_25);
                            add(VehicleType.TT_39);
                            add(VehicleType.MCH);
                            add(VehicleType.TRL);
                            add(VehicleType.TLNK);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                }
            }
            ));

            testLanes.add(new StandardLane("L19", "3.DECK", 3,
                    -12.95f, 9.9f, 15f, 125f, 10,0, LaneCoefficient.P31.getLaneCoefficient(),LaneCoefficient.P31.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.FULL);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);

                            add(VehicleType.VAN);
                            add(VehicleType.VAN_H);
                            add(VehicleType.L_L);
                            add(VehicleType.L_MH);
                            add(VehicleType.BUS);
                            add(VehicleType.LOR);
                            add(VehicleType.TT_25);
                            add(VehicleType.TT_39);
                            add(VehicleType.MCH);
                            add(VehicleType.TRL);
                            add(VehicleType.TLNK);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                }
            }
            ));

            ///  DECK 5

            testLanes.add(new StandardLane("L21", "5.DECK", 5,
                    12.85f, 15.3f, 0.0f, 168.0f, 11,0, LaneCoefficient.P51.getLaneCoefficient(),LaneCoefficient.P51.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.FULL);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);

                            add(VehicleType.VAN);
                            add(VehicleType.VAN_H);
                            add(VehicleType.L_L);
                            add(VehicleType.L_MH);
                            add(VehicleType.BUS);
                            add(VehicleType.LOR);
                            add(VehicleType.TT_25);
                            add(VehicleType.TT_39);
                            add(VehicleType.MCH);
                            add(VehicleType.TRL);
                            add(VehicleType.TLNK);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                }
            }
            ));

            testLanes.add(new StandardLane("L22", "5.DECK", 5,
                    9.75f, 15.3f, 13f, 157f,12, 0, LaneCoefficient.P52.getLaneCoefficient(),LaneCoefficient.P52.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.FULL);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);

                            add(VehicleType.VAN);
                            add(VehicleType.VAN_H);
                            add(VehicleType.L_L);
                            add(VehicleType.L_MH);
                            add(VehicleType.BUS);
                            add(VehicleType.LOR);
                            add(VehicleType.TT_25);
                            add(VehicleType.TT_39);
                            add(VehicleType.MCH);
                            add(VehicleType.TRL);
                            add(VehicleType.TLNK);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                }
            }
            ));

            testLanes.add(new StandardLane("L23A", "5.DECK", 5,
                    6.2f, 15.3f, 20f, 36f, 13,0, LaneCoefficient.P54.getLaneCoefficient(),LaneCoefficient.P51.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.STERN);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);

                            add(VehicleType.VAN);
                            add(VehicleType.VAN_H);
                            add(VehicleType.L_L);
                            add(VehicleType.L_MH);
                            add(VehicleType.BUS);
                            add(VehicleType.LOR);
                            add(VehicleType.TT_25);
                            add(VehicleType.TT_39);
                            add(VehicleType.MCH);
                            add(VehicleType.TRL);
                            add(VehicleType.TLNK);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                }
            }
            ));

            testLanes.add(new StandardLane("L23M", "5.DECK", 5,
                    6.2f, 15.3f, 62.6f, 105.6f, 14, 0, LaneCoefficient.P54.getLaneCoefficient(),LaneCoefficient.P54.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.MIDDLE);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);

                            add(VehicleType.VAN);
                            add(VehicleType.VAN_H);
                            add(VehicleType.L_L);
                            add(VehicleType.L_MH);
                            add(VehicleType.BUS);
                            add(VehicleType.LOR);
                            add(VehicleType.TT_25);
                            add(VehicleType.TT_39);
                            add(VehicleType.MCH);
                            add(VehicleType.TRL);
                            add(VehicleType.TLNK);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                }
            }
            ));

            testLanes.add(new StandardLane("L23F", "5.DECK", 5,
                    6.2f, 15.3f, 146f, 152f,15, 0, LaneCoefficient.P54.getLaneCoefficient(),LaneCoefficient.P55.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.BOW);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);

                            add(VehicleType.VAN);
                            add(VehicleType.VAN_H);
                            add(VehicleType.L_L);
                            add(VehicleType.L_MH);
                            add(VehicleType.BUS);
                            add(VehicleType.LOR);
                            add(VehicleType.TT_25);
                            add(VehicleType.TT_39);
                            add(VehicleType.MCH);
                            add(VehicleType.TRL);
                            add(VehicleType.TLNK);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                }
            }
            ));

            testLanes.add(new StandardLane("L24A", "5.DECK", 5,
                    3.45f, 15.3f, 0.0f, 28f, 16,0, LaneCoefficient.P54.getLaneCoefficient(),LaneCoefficient.P55.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.STERN);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);

                            add(VehicleType.VAN);
                            add(VehicleType.VAN_H);
                            add(VehicleType.L_L);
                            add(VehicleType.L_MH);
                            add(VehicleType.BUS);
                            add(VehicleType.LOR);
                            add(VehicleType.TT_25);
                            add(VehicleType.TT_39);
                            add(VehicleType.MCH);
                            add(VehicleType.TRL);
                            add(VehicleType.TLNK);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                }
            }
            ));

            testLanes.add(new StandardLane("L24F", "5.DECK", 5,
                    3.45f, 15.3f, 149f, 180.0f,17, 0, LaneCoefficient.P54.getLaneCoefficient(), LaneCoefficient.P51.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.STERN);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);

                            add(VehicleType.VAN);
                            add(VehicleType.VAN_H);
                            add(VehicleType.L_L);
                            add(VehicleType.L_MH);
                            add(VehicleType.BUS);
                            add(VehicleType.LOR);
                            add(VehicleType.TT_25);
                            add(VehicleType.TT_39);
                            add(VehicleType.MCH);
                            add(VehicleType.TRL);
                            add(VehicleType.TLNK);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                }
            }
            ));

            testLanes.add(new StandardLane("L24MP", "5.DECK", 5,
                    3.8f, 15.3f, 94.6f, 105.6f, 18,0, LaneCoefficient.P55.getLaneCoefficient(),LaneCoefficient.P55.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.MIDDLE);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);

                            add(VehicleType.VAN);
                            add(VehicleType.VAN_H);
                            add(VehicleType.L_L);
                            add(VehicleType.L_MH);
                            add(VehicleType.BUS);
                            add(VehicleType.LOR);
                            add(VehicleType.TT_25);
                            add(VehicleType.TT_39);
                            add(VehicleType.MCH);
                            add(VehicleType.TRL);
                            add(VehicleType.TLNK);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                }
            }
            ));

            testLanes.add(new StandardLane("L24MS", "5.DECK", 5,
                    1.4f, 15.3f, 94.6f, 105.6f, 19,0, LaneCoefficient.P55.getLaneCoefficient(), LaneCoefficient.P55.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.MIDDLE);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);

                            add(VehicleType.VAN);
                            add(VehicleType.VAN_H);
                            add(VehicleType.L_L);
                            add(VehicleType.L_MH);
                            add(VehicleType.BUS);
                            add(VehicleType.LOR);
                            add(VehicleType.TT_25);
                            add(VehicleType.TT_39);
                            add(VehicleType.MCH);
                            add(VehicleType.TRL);
                            add(VehicleType.TLNK);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                }
            }
            ));

            testLanes.add(new StandardLane("L25A", "5.DECK", 5,
                    -0.9f, 15.3f, 0.0f, 32.5f, 20,0, LaneCoefficient.P55.getLaneCoefficient(),LaneCoefficient.P51.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.STERN);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);

                            add(VehicleType.VAN);
                            add(VehicleType.VAN_H);
                            add(VehicleType.L_L);
                            add(VehicleType.L_MH);
                            add(VehicleType.BUS);
                            add(VehicleType.LOR);
                            add(VehicleType.TT_25);
                            add(VehicleType.TT_39);
                            add(VehicleType.MCH);
                            add(VehicleType.TRL);
                            add(VehicleType.TLNK);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                }
            }
            ));

            testLanes.add(new StandardLane("L25F", "5.DECK", 5,
                    -0.9f, 15.3f, 129.0f, 162f,21, 0, LaneCoefficient.P55.getLaneCoefficient(),LaneCoefficient.P55.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.BOW);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.VAN);

                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }
            ));

            testLanes.add(new StandardLane("L25M", "5.DECK", 5,
                    -0.9f, 15.3f, 32.5f, 129.0f, 22, 0, LaneCoefficient.P55.getLaneCoefficient(),LaneCoefficient.P55.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.BOW);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.VAN);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }
            ));

            testLanes.add(new StandardLane("L26A", "5.DECK", 5,
                    -3.3f, 15.3f, 14f, 49f, 23,0, LaneCoefficient.P51.getLaneCoefficient(),LaneCoefficient.P51.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.STERN);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);

                            add(VehicleType.VAN);
                            add(VehicleType.VAN_H);
                            add(VehicleType.L_L);
                            add(VehicleType.L_MH);
                            add(VehicleType.BUS);
                            add(VehicleType.LOR);
                            add(VehicleType.TT_25);
                            add(VehicleType.TT_39);
                            add(VehicleType.MCH);
                            add(VehicleType.TRL);
                            add(VehicleType.TLNK);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                }
            }
            ));

            testLanes.add(new StandardLane("L26F", "5.DECK", 5,
                    -3.3f, 15.3f, 129.0f, 157.0f,24, 0, LaneCoefficient.P52.getLaneCoefficient(),LaneCoefficient.P55.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.BOW);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.VAN);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }
            ));

            testLanes.add(new StandardLane("L26M", "5.DECK", 5,
                    -3.3f, 15.3f, 49f, 129.0f, 25,0, LaneCoefficient.P54.getLaneCoefficient(),LaneCoefficient.P54.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.MIDDLE);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.VAN);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }
            ));

            testLanes.add(new StandardLane("L27A", "5.DECK", 5,
                    -5.7f, 15.3f, 22f, 49f,26, 0, LaneCoefficient.P51.getLaneCoefficient(),LaneCoefficient.P51.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.STERN);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.VAN);
                            add(VehicleType.VAN_H);
                            add(VehicleType.L_L);
                            add(VehicleType.L_MH);
                            add(VehicleType.BUS);
                            add(VehicleType.LOR);
                            add(VehicleType.TT_25);
                            add(VehicleType.TT_39);
                            add(VehicleType.MCH);
                            add(VehicleType.TRL);
                            add(VehicleType.TLNK);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                }
            }
            ));

            testLanes.add(new StandardLane("L27F", "5.DECK", 5,
                    -5.7f, 15.3f, 129.0f, 152.0f,27, 0, LaneCoefficient.P51.getLaneCoefficient(),LaneCoefficient.P55.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.STERN);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.VAN);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }
            ));

            testLanes.add(new StandardLane("L27M", "5.DECK", 5,
                    -5.7f, 15.3f, 49.0f, 129.0f,28, 0, LaneCoefficient.P53.getLaneCoefficient(),LaneCoefficient.P53.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.MIDDLE);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.VAN);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }
            ));

            testLanes.add(new StandardLane("L28", "5.DECK", 5,
                    -10.05f, 15.3f, 0.0f, 184.0f, 29,0, LaneCoefficient.P52.getLaneCoefficient(),LaneCoefficient.P52.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.FULL);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);

                            add(VehicleType.VAN);
                            add(VehicleType.VAN_H);
                            add(VehicleType.L_L);
                            add(VehicleType.L_MH);
                            add(VehicleType.BUS);
                            add(VehicleType.LOR);
                            add(VehicleType.TT_25);
                            add(VehicleType.TT_39);
                            add(VehicleType.MCH);
                            add(VehicleType.TRL);
                            add(VehicleType.TLNK);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                }
            }
            ));

            testLanes.add(new StandardLane("L29", "5.DECK", 5,
                    -13.15f, 15.3f, 0.0f, 184.0f, 30,0, LaneCoefficient.P51.getLaneCoefficient(),LaneCoefficient.P51.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.FULL);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);

                            add(VehicleType.VAN);
                            add(VehicleType.VAN_H);
                            add(VehicleType.L_L);
                            add(VehicleType.L_MH);
                            add(VehicleType.BUS);
                            add(VehicleType.LOR);
                            add(VehicleType.TT_25);
                            add(VehicleType.TT_39);
                            add(VehicleType.MCH);
                            add(VehicleType.TRL);
                            add(VehicleType.TLNK);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                }
            }
            ));

            // 6 DECK

            testLanes.add(new StandardLane("L31", "6.DECK", 6,
                    3.25f, 18.15f, 94.8f, 124.8f,31, 0, LaneCoefficient.P64.getLaneCoefficient(),LaneCoefficient.P64.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.FULL);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                    add(VehicleType.VAN);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }
            ));

            testLanes.add(new StandardLane("L32", "6.DECK", 6,
                    0.95f, 18.15f, 94.8f, 124.8f,32, 0, LaneCoefficient.P64.getLaneCoefficient(), LaneCoefficient.P64.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.FULL);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                        }
                    }, new ArrayList<>() {
                {
                    add(VehicleType.SHP_CAR);
                    add(VehicleType.VAN);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }
            ));

            testLanes.add(new StandardLane("L33", "6.DECK", 6,
                    -1.35f, 18.15f, 47.4f, 147.4f,33, 0, LaneCoefficient.P62.getLaneCoefficient(),LaneCoefficient.P62.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.FULL);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);


                        }
                    },
                    new ArrayList<>() {
                        {
                            // not allowed
                            add(VehicleType.SHP_CAR);
                            add(VehicleType.VAN);
                            add(VehicleType.VAN_H);
                            add(VehicleType.L_L);
                            add(VehicleType.L_MH);
                            add(VehicleType.BUS);
                            add(VehicleType.LOR);
                            add(VehicleType.TT_25);
                            add(VehicleType.TT_39);
                            add(VehicleType.MCH);
                            add(VehicleType.TRL);
                            add(VehicleType.TLNK);
                        }
                    }
            ));

            testLanes.add(new StandardLane("L34", "6.DECK", 6,
                    -3.65f, 18.15f, 28.7f, 147.4f, 34,0,  LaneCoefficient.P64.getLaneCoefficient(),LaneCoefficient.P64.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.FULL);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.SHP_CAR);

                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.VAN);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }
            ));

            testLanes.add(new StandardLane("L35", "6.DECK", 6,
                    -5.95f, 18.15f, 28.7f, 147.4f,35, 0, LaneCoefficient.P61.getLaneCoefficient(),LaneCoefficient.P61.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.FULL);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);


                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.SHP_CAR);
                    add(VehicleType.VAN);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }
            ));

            // ********************************** DECK 7 ********************************************

            testLanes.add(new StandardLane("L41", "7.DECK", 7,
                    13.2f, 21f, 105.3f, 167.3f, 36,0,LaneCoefficient.P71.getLaneCoefficient(),LaneCoefficient.P74.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.FULL);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.SHP_CAR);

                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.VAN);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }
            ));


            testLanes.add(new StandardLane("L42", "7.DECK", 7,
                    9.15f, 21f, 82.8f, 169.3f,37, 0,LaneCoefficient.P71.getLaneCoefficient(),LaneCoefficient.P73.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.FULL);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.SHP_CAR);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.VAN);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }
            ));

            testLanes.add(new StandardLane("L42A", "7.DECK", 7,
                    11.2f, 21f, 53.7f, 58.6f, 38,0,LaneCoefficient.P74.getLaneCoefficient(),LaneCoefficient.P71.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.STERN);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.SHP_CAR);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.VAN);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }));

            testLanes.add(new StandardLane("L43A", "7.DECK", 7,
                    6.1f, 21f, 54.2f, 59f,39, 0,LaneCoefficient.P74.getLaneCoefficient(),LaneCoefficient.P71.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.STERN);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.SHP_CAR);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.VAN);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }));

            testLanes.add(new StandardLane("L43M", "7.DECK", 7,
                    6.1f, 21f, 83f, 102.7f, 40,0,LaneCoefficient.P73.getLaneCoefficient(),LaneCoefficient.P73.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.MIDDLE);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.SHP_CAR);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.VAN);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }));

            testLanes.add(new StandardLane("L43F1", "7.DECK", 7,
                    6.65f, 21f, 131f, 169.3f, 41,0,LaneCoefficient.P72.getLaneCoefficient(),LaneCoefficient.P74.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.BOW);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.SHP_CAR);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.VAN);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }));

            testLanes.add(new StandardLane("L43F2", "7.DECK", 7,
                    4.9f, 21f, 170f, 177f, 42,0,LaneCoefficient.P71.getLaneCoefficient(),LaneCoefficient.P74.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.BOW);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.SHP_CAR);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.VAN);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }));

            testLanes.add(new StandardLane("L44A", "7.DECK", 7,
                    4.7f, 21f, 47.2f, 52f, 43,0 , LaneCoefficient.P73.getLaneCoefficient(), LaneCoefficient.P71.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.STERN);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.SHP_CAR);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.VAN);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }));

            testLanes.add(new StandardLane("L44M", "7.DECK", 7,
                    2.5f,21f, 99f,119.2f, 44,0, LaneCoefficient.P74.getLaneCoefficient(), LaneCoefficient.P74.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.MIDDLE);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.SHP_CAR);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.VAN);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }));

            testLanes.add(new StandardLane("L44F1", "7.DECK", 7,
                    1.8f, 21f, 150.7f, 155.7f, 45, 0,LaneCoefficient.P72.getLaneCoefficient(),LaneCoefficient.P74.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.BOW);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.SHP_CAR);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.VAN);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }));

            testLanes.add(new StandardLane("L44F2", "7.DECK", 7,
                    1.8f, 21f, 160f, 165f, 46,0, LaneCoefficient.P72.getLaneCoefficient(), LaneCoefficient.P74.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.BOW);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.SHP_CAR);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.VAN);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }));
            testLanes.add(new StandardLane("L45A", "7.DECK", 7,
                    -1.55f, 21f, 52f, 72f,47, 0,LaneCoefficient.P74.getLaneCoefficient(),LaneCoefficient.P71.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.STERN);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.SHP_CAR);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.VAN);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }));

            testLanes.add(new StandardLane("L45M", "7.DECK", 7,
                    -1.45f, 21f, 95.4f, 125.4f,48, 0,LaneCoefficient.P74.getLaneCoefficient(),LaneCoefficient.P74.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.STERN);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.SHP_CAR);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.VAN);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }));

            testLanes.add(new StandardLane("L46A", "7.DECK", 7,
                    -4.05f, 21f, 47f,79.5f,49, 0,LaneCoefficient.P73.getLaneCoefficient(),LaneCoefficient.P71.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.BOW);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.SHP_CAR);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.VAN);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }));

            testLanes.add(new StandardLane("L46M", "7.DECK", 7,
                    -3.95f, 21f, 95.4f,125.4f, 50, 0,LaneCoefficient.P74.getLaneCoefficient(),LaneCoefficient.P71.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.MIDDLE);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.SHP_CAR);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.VAN);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }));

            testLanes.add(new StandardLane("L46F1", "7.DECK", 7,
                    -4.2f, 21f, 150.7f, 155.7f,51, 0,LaneCoefficient.P73.getLaneCoefficient(),LaneCoefficient.P74.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.BOW);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.SHP_CAR);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.VAN);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }));

            testLanes.add(new StandardLane("L46F2", "7.DECK", 7,
                    -4.2f, 215f, 160f, 165f, 52,0,LaneCoefficient.P71.getLaneCoefficient(),LaneCoefficient.P74.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.BOW);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.SHP_CAR);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.VAN);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }));

            testLanes.add(new StandardLane("L46F3", "7.DECK", 7,
                    -2.5f, 21f, 170f, 177f, 53,0,LaneCoefficient.P71.getLaneCoefficient(),LaneCoefficient.P74.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.BOW);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.SHP_CAR);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.VAN);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }));

            testLanes.add(new StandardLane("L47A", "7.DECK", 7,
                    -6.55f, 21f, 47f, 138.8f,54, 0,LaneCoefficient.P73.getLaneCoefficient(),LaneCoefficient.P71.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.STERN);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.SHP_CAR);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.VAN);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }));

            testLanes.add(new StandardLane("L48A", "7.DECK", 7,
                    -11.05f, 21f, 53.7f, 58.6f, 55,0,LaneCoefficient.P74.getLaneCoefficient(),LaneCoefficient.P71.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.FULL);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.SHP_CAR);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.VAN);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }));

            testLanes.add(new StandardLane("L48", "7.DECK", 7,
                    -10.75f, 21f, 79f, 165.6f, 56,0,LaneCoefficient.P71.getLaneCoefficient(),LaneCoefficient.P73.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.FULL);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.SHP_CAR);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.VAN);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }));

            testLanes.add(new StandardLane("L49", "7.DECK", 7,
                    -13.25f, 21f, 105.6f, 165.6f, 57,0, LaneCoefficient.P71.getLaneCoefficient(),LaneCoefficient.P74.getLaneCoefficient(),
                    new ArrayList<>() {
                        {
                            // allowed
                            add(LaneLocation.FULL);
                        }
                    },
                    new ArrayList<>() {
                        {
                            // allowed
                            add(VehicleType.MC);
                            add(VehicleType.CAR);
                            add(VehicleType.SHP_CAR);
                        }
                    }, new ArrayList<>() {
                {
                    // not allowed
                    add(VehicleType.VAN);
                    add(VehicleType.VAN_H);
                    add(VehicleType.L_L);
                    add(VehicleType.L_MH);
                    add(VehicleType.BUS);
                    add(VehicleType.LOR);
                    add(VehicleType.TT_25);
                    add(VehicleType.TT_39);
                    add(VehicleType.MCH);
                    add(VehicleType.TRL);
                    add(VehicleType.TLNK);
                }
            }));


        }

    }

    List<Lane> getSourceLanes() {
        return testLanes;
    }

}
