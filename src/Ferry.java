import java.util.*;

public class Ferry implements Ship {

    List<Lane> lanes;
    List<Record> outputTable;
    List<LoadRecord> outputLoadTable;
    String exportTableHeader = "\nTAB*OB_ROROLOAD\n" +
            "LOAD,ID,LANE,PART,LPORT,DPORT,EXTRA,LDES,L,B,HS,H,LENX,LENY,LENZ,XMIN,X1,XMAX,X2,STATI,MASS,XM,YM,ZM,STATUS,YMIN,YMAX,ZMIN,ZMAX,IMO,UNNO,EMS,PGR,MFAG,IMDG,MP,LQ,W,W1,RNR,W2,W2MAX,AREA,VOL,STF,LFCODE,OPTION,NR,N1,UNITW,LAYER,HMAX,HDECK,WD,ERROR";
    String exportLoadTableHeader = "\nTAB*OB_RRBOOKLIST\nID,PDES,LDES,+,LOAD,TYPE,LPORT,DPORT,EXTRA,LENX,L,LENY,B,HS,LENZ,UNITW,IMO,UNNO,EMS,MFAG,IMDG,PGR,MP,W,LQ,NR,NL,PCS,STATUS,LFCODE,W1,W2,STF,MASSL,MODE,MASS,MASSH,CAS,HDECK,ACODE";
    static boolean direction;
    private boolean error = false;
    static float shipX = 88.13f;

    public Ferry(List<Lane> lanes) {
        this.lanes = lanes;
        outputTable= new LinkedList<>();
        outputLoadTable = new LinkedList<>();
        direction=true;
    }

    public static boolean  getDirection ()
    {
        return direction;
    }

    public static void setDirection (boolean thisDirection)
    {
        direction=thisDirection;
    }

    @Override

    public int loadVehicle(Vehicle vehicle, Lane targetLane) throws Exception {
        {
            try {
                if (vehicle != null && ( vehicle.getVehicleLength() <= targetLane.getAvailableLength()  )) {
                    targetLane.addVehicleToLane(vehicle, direction);

                }
                return 1;
            } catch (Exception e) {
                throw new Exception("No room for this vehicle: " + vehicle.getRegistrationNumber());
            }
        }

    }

    @Override
    public void unloadVehicle(Vehicle vehicle) {

    }

    @Override
    public void moveVehicles(boolean direction) {

    }

    @Override
    public boolean getError ()
    {
        return error;
    }

    @Override
    public void setError(boolean b) {
        error=true;
    }


    public String getExportTableHeader() { return exportTableHeader;}

    public String getExportLoadTableHeader() { return exportLoadTableHeader;}


    public List<Lane> getLanes() {
        return lanes;
    }


    public void viewAllLanes() {
        MainFrame.addData("\n Viewing all lanes");
        for (Lane currentLane : lanes) {
            MainFrame.addData(currentLane.toString());
        }
    }

    public void sortLanes() {
        Collections.sort(lanes, new Comparator<Lane>() {
            @Override
            public int compare(Lane o1, Lane t1) {
                return (int)(t1.getPriority() - o1.getPriority());
            }
        });
    }

    public void sortToDefault() {
        Collections.sort(lanes, new Comparator<Lane>() {
            @Override
            public int compare(Lane o1, Lane t1) {
                return (o1.getDefaultPriority() - t1.getDefaultPriority());
            }
        });
    }

    public void sortByName() {
        Collections.sort(lanes, new Comparator<Lane>() {
            @Override
            public int compare(Lane o1, Lane t1) {
                return (t1.getName().compareTo(o1.getName()));
            }
        });
    }


    public List<Record> getExportTable ()
    {
        return outputTable;
    }
    public List<LoadRecord> getExportLoadTable ()
    {
        return outputLoadTable;
    }

    public void calculatePriorityLanes() {
        if (direction)
        {

            for (Lane currentLane :lanes)
            {
                currentLane.setPriority(currentLane.getLaneCoefficient()*currentLane.getAvailableLength());
            }
        }
        else
        {
            for (Lane currentLane :lanes)
            {
                currentLane.setPriority(currentLane.getLaneCoefficientReversed()*(currentLane.getAvailableLength()));
            }
        }
    }
    public void markLanes (Vehicle vehicle)
    {
        for (Lane currentLane:lanes)
        {
            if (currentLane.getNotAllowedVehicleTypes().contains(vehicle.getVehicleType()))
                currentLane.setPriority(-1);
            else if (currentLane.getAvailableLength() < vehicle.getVehicleLength())
                currentLane.setPriority(-1);

        }
    }

    public Lane getBestLane() {
        return lanes.get(0);
    }

    public void moveVehicles ()
    {

    }

    public boolean roomCheck() {
        int error=0;
        for (Lane currentLane :lanes)
        {
            if (currentLane.getPriority()==-1)
                error+=1;
        }
        if (error==57) {
            setError(true);
            return false;
        }
        else return true;
    }

    public void exportToLoadTable(List<VehicleType> listVehicle, List<LoadRecord> outputTable)
    {
        for (VehicleType currentVehicleType: listVehicle)
        {
            LoadRecord tempRecord = new LoadRecord();
            tempRecord.setHEADER("");
            tempRecord.setID(currentVehicleType.name());
            tempRecord.setPDES(currentVehicleType.getVehicleDescription());
            tempRecord.setLDES(currentVehicleType.getVehicleDescription());
            tempRecord.setPLUS("");
            tempRecord.setLOAD(currentVehicleType.name());
            tempRecord.setTYPE();
            tempRecord.setLPORT("");
            tempRecord.setDPORT("");
            tempRecord.setEXTRA("");
            tempRecord.setLENX(currentVehicleType.getVehicleLength());
            tempRecord.setL(currentVehicleType.getVehicleLength());
            tempRecord.setLENY(currentVehicleType.getVehicleWidth());
            tempRecord.setB(currentVehicleType.getVehicleWidth());
            tempRecord.setHS(0);
            tempRecord.setLENZ(currentVehicleType.getVehicleHeight());
            tempRecord.setUNITW(currentVehicleType.getVehicleMass());
            tempRecord.setIMO("");
            tempRecord.setUNNO("");
            tempRecord.setEMS("");
            tempRecord.setMFAG("");
            tempRecord.setIMDG("");
            tempRecord.setPGR("");
            tempRecord.setMP("");
            tempRecord.setW(0);
            tempRecord.setLQ("");
            tempRecord.setNR(999);
            tempRecord.setNL(currentVehicleType.getTotalType());
            tempRecord.setPCS(999-currentVehicleType.getTotalType());
            tempRecord.setSTATUS("");
            tempRecord.setLFCODE(currentVehicleType.getVehicleColour());
            tempRecord.setW1(0);
            tempRecord.setW2(0);
            tempRecord.setSTF(0);
            tempRecord.setMASSL(currentVehicleType.getTotalType()*currentVehicleType.getVehicleMass());
            tempRecord.setMODE("");
            tempRecord.setMASS(999*currentVehicleType.getVehicleMass());
            tempRecord.setMASSH((999*currentVehicleType.getVehicleMass()) - (currentVehicleType.getTotalType()*currentVehicleType.getVehicleMass()));
            tempRecord.setCAS("");
            tempRecord.setHDECK(0);
            tempRecord.setACODE("");

            outputTable.add(tempRecord);
        }
    }

}

