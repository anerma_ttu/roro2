import java.util.List;

public enum VehicleType {
    MC(1.0f, 1.0f, 2.0f, 0.2f,"MOTORBIKE", VehicleQueue.getNumMC(),"YELLOW"),
    CAR(2.1f, 1.9f, 4.8f, 1.8f,"CAR", VehicleQueue.getNumCAR(),"DARK_GREEN"),
    SHP_CAR(2.1f, 1.9f, 4.8f, 1.8f,"SHIPPING CAR", VehicleQueue.getNumSHP_CAR(),"TEAL"),
    VAN(2.1f, 2.25f, 7.0f, 2.5f,"VAN", VehicleQueue.getNumVAN(),"PALE_BLUE"),
    VAN_H(2.1f, 2.4f, 7.0f, 2.5f,"VAN HIGH", VehicleQueue.getNumVAN_H(),"DARK_BLUE"),
    L_L(2.1f, 1.9f, 10.0f, 5f,"LONG&LOW", VehicleQueue.getNumL_L(),"INDIGO"),
    L_MH(2.1f, 4.4f, 10.0f, 5.0f,"LONG&MEDIUM/HIGH",VehicleQueue.getNumL_MH(),"PERIWINKLE"),
    BUS(2.7f, 4.4f, 14.0f, 12.0f,"BUS",VehicleQueue.getNumBUS(),"DARK_PURPLE"),
    LOR(2.7f, 4.1f, 11.0f, 12.0f,"LORRY",VehicleQueue.getNumLOR(),"GRAY"),
    TT_25(2.7f, 4.1f, 17.0f, 25.0f,"TRUCK/TRAILER", VehicleQueue.getNumTT_25(),"ORANGE"),
    TT_39(2.7f, 4.1f, 17.0f, 39.0f,"TRUCK/TRAILER", VehicleQueue.getNumTT_39(),"ORANGE"),
    MCH(2.7f, 4.1f, 10.0f, 15f,"MACHINE", VehicleQueue.getNumMCH(),"OLIVE_GREEN"),
    TRL(2.7f, 4.1f, 14.0f, 15f,"TRAILER", VehicleQueue.getNumTRL(),"DARK_RED"),
    TLNK(2.7f, 4.1f, 17.0f, 1f,"TALLINK TRAILER", VehicleQueue.getNumTLNK(),"WHITE");

    private float vehicleWidth;
    private float vehicleHeight;
    private float vehicleLength;
    private float vehicleMass;
    private String vehicleDescription;
    private int vehicleTotalType;
    private String vehicleColour;

    VehicleType(float vehicleWidth,
                float vehicleHeight,
                float vehicleLength,
                float vehicleMass,
                String vehicleDescription,
                int vehicleTotalType,
                String vehicleColour
    ) {
        this.vehicleWidth = vehicleWidth;
        this.vehicleHeight = vehicleHeight;
        this.vehicleLength = vehicleLength;
        this.vehicleMass = vehicleMass;
        this.vehicleDescription = vehicleDescription;
        this.vehicleTotalType = vehicleTotalType;
        this.vehicleColour = vehicleColour;
    }


    public float getVehicleWidth() {
        return vehicleWidth;
    }

    public float getVehicleHeight() {
        return vehicleHeight;
    }

    public String getVehicleDescription() {
        return vehicleDescription;
    }

    public float getVehicleLength() {

        return vehicleLength;
    }

    public float getVehicleMass() {
        return vehicleMass;
    }

    public int getTotalType () {return vehicleTotalType;}

    public String getVehicleColour () {return vehicleColour;}



}

