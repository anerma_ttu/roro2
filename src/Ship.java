

import java.util.List;

public interface Ship {
    /**
     * @throws Exception if ship if no lane found
     */

    /**
     * @throws Exception if ship if full
     */

    // lane selection

    Lane getBestLane();

    List<Lane> getLanes ();

    // lane calculation
    public void calculatePriorityLanes();

    void markLanes(Vehicle vehicle);

    void sortLanes();

    // load and unload vehicle
    int loadVehicle (Vehicle vehicle, Lane lane) throws Exception;

    void unloadVehicle (Vehicle vehicle);

    void moveVehicles(boolean direction);

    // lane view

    void viewAllLanes();

    // table export

    void sortToDefault();

    void sortByName();

    List<Record> getExportTable();

    List<LoadRecord> getExportLoadTable();

    void exportToLoadTable(List<VehicleType> listVehicle, List<LoadRecord> outputTable);

    String getExportTableHeader();

    String getExportLoadTableHeader();

    boolean roomCheck();

    boolean getError();

    void setError (boolean b);
}
