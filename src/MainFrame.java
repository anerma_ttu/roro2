import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.*;

public class MainFrame extends JFrame {



    private static final long serialVersionUID = -5766323791092320305L;
    private JPanel contentPane;

    private JPanel logBook;
    private JPanel filePanel;

    private JPanel routePanel;
    private JPanel processPanel;
    private JPanel exitPanel;
    // private JPanel connectionPanel;

    private JScrollPane scrollPanel;
    private static JTextArea textArea;

    private static JRadioButton directionTH;
    private static JRadioButton directionHT;
    private static JButton fileOpenButton;
    // private static JButton fileCloseButton;
    private static JButton startProcessButton;
    private static JButton exitButton;
    // end of members

    public MainFrame() {
        //ship.addVehicles(SourceVehicles.getVehicles());

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setBounds(100, 100, 500, 800);
        this.setMinimumSize(new Dimension(430, 600));
        this.setName("RORO2r");

        // content [pane properties
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
        setContentPane(contentPane);

        logBook = new JPanel();
        logBook.setBorder(BorderFactory.createTitledBorder("Logbook RORO2"));
        logBook.setPreferredSize(new Dimension(300, 400));
        logBook.setMinimumSize(new Dimension(250, 300));


        // filePanel

        filePanel = new JPanel();
        filePanel.setPreferredSize(new Dimension(300, 60));
        filePanel.setMinimumSize(new Dimension(250, 60));
        filePanel.setBorder(BorderFactory.createTitledBorder("File panel"));

        //selectionPanel
        routePanel = new JPanel();
        routePanel.setPreferredSize(new Dimension(300, 60));
        routePanel.setMinimumSize(new Dimension(250, 60));
        routePanel.setBorder(BorderFactory.createTitledBorder("Route panel"));
        // process panel
        processPanel = new JPanel();
        processPanel.setPreferredSize(new Dimension(300, 60));
        processPanel.setMinimumSize(new Dimension(250, 60));
        filePanel.setBorder(BorderFactory.createTitledBorder("Process panel"));
        // exit panel

        exitPanel = new JPanel();
        exitPanel.setPreferredSize(new Dimension(300, 40));
        exitPanel.setMinimumSize(new Dimension(250, 30));

        textArea = new JTextArea();
        textArea.setEditable(false);
        scrollPanel = new JScrollPane();
        scrollPanel.setViewportView(textArea);
        scrollPanel.createVerticalScrollBar();
        scrollPanel.setMinimumSize(new Dimension(100, 100));
        scrollPanel.setBorder(new EmptyBorder(5, 5, 5, 5));

        scrollPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        // buttons
        //setStatus(new ButtonControl());
        // processControl = new ProcessControl();
        SourceLanes testLanes = new SourceLanes();
        testLanes.initLanes();
        // lanes init
        Ship ship = new Ferry(testLanes.getSourceLanes());
        VehicleQueue initialQueue = new VehicleQueue(ship);
        InputData setter = new InputData();
        fileOpenButton = new JButton("Open file");
        fileOpenButton.setEnabled(true);
        fileOpenButton.addActionListener(setter);
        fileOpenButton.setPreferredSize(new Dimension(100, 20));

        //fileCloseButton = new JButton("Close file");
        //fileCloseButton.setEnabled(false);
        //fileCloseButton.addActionListener(setter);
        //fileCloseButton.setPreferredSize(new Dimension(100, 20));


        // route pane buttons

        directionTH = new JRadioButton("HEL-TAL", true);
        directionHT = new JRadioButton("TAL-HEL", false);

        ButtonGroup directionGroup = new ButtonGroup();
        directionGroup.add(directionTH);
        directionGroup.add(directionHT);


        // process pane buttons
        startProcessButton = new JButton("Start");
        startProcessButton.setEnabled(true);
        startProcessButton.addActionListener(initialQueue);
        startProcessButton.setPreferredSize(new Dimension(100, 20));


        /*
        disconnectButton = new JButton("Disconnect");
        disconnectButton.setEnabled(false);
        disconnectButton.addActionListener(getStatus());
        disconnectButton.addActionListener(processControl);
        disconnectButton.setPreferredSize(new Dimension(100, 20));

        startProcessButton = new JButton("Start");
        startMeasureButton.setEnabled(false);
        startMeasureButton.addActionListener(getStatus());
        startMeasureButton.addActionListener(processControl);
        startMeasureButton.setPreferredSize(new Dimension(100, 20));

        stopMeasureButton = new JButton("Break");
        stopMeasureButton.setEnabled(false);
        stopMeasureButton.addActionListener(getStatus());
        stopMeasureButton.addActionListener(processControl);
        stopMeasureButton.setPreferredSize(new Dimension(100, 20));
        */

        exitButton = new JButton("Exit");
        exitButton.setPreferredSize(new Dimension(100, 20));
        exitButton.addActionListener(new CloseListener());


        // adding to panels
        logBook.setLayout(new BorderLayout());
        logBook.add(scrollPanel, BorderLayout.CENTER);


        // adding to filePanel
        filePanel.add(fileOpenButton);
        //filePanel.add(fileCloseButton);
        filePanel.setBorder(BorderFactory.createTitledBorder("Open file"));

        // adding to routePanel

        routePanel.add(directionTH);
        routePanel.add(directionHT);
        pack();
        routePanel.setBorder(BorderFactory.createTitledBorder("Select route"));



        // adding to connectionPanel
        processPanel.add(startProcessButton);
        //connectionPanel.add(disconnectButton);
        processPanel.setBorder(BorderFactory.createTitledBorder("Start processing"));

        //adding to measurementPanel
        // measurementPanel.add(startMeasureButton);
        //measurementPanel.add(stopMeasureButton);
        //measurementPanel.setBorder(BorderFactory.createTitledBorder("Measurements"));

        //adding to exitPanel
        exitPanel.add(exitButton);

        // adding to contentPane

        contentPane.add(logBook);
        contentPane.add(filePanel);
        contentPane.add(processPanel);
        //contentPane.add(measurementPanel);
        contentPane.add(exitPanel);

        // showing on the content pane
        this.getContentPane().add(logBook);
        this.getContentPane().add(filePanel);
        this.getContentPane().add(routePanel);
        this.getContentPane().add(processPanel);
        //this.getContentPane().add(measurementPanel);
        this.getContentPane().add(exitPanel);
        this.deactivateStartProcessButton();
    }

    public static void addData(String data) {
        textArea.append(data);
        //add to file
    }

    // File Open

    public static void deactivateFileOpenButton() {
        fileOpenButton.setEnabled(false);
    }

    public static void activateFileOpenButton() {
        fileOpenButton.setEnabled(true);
    }

    public static boolean getRoute ()
    {
        return directionTH.isSelected();
    }

    /*
    public static void activateStartButton() {
        fileCloseButton.setEnabled(false);
    }

    public static void activateFileCloseButton() {
        fileCloseButton.setEnabled(true);
    }
    */
    // Connect
    public static void deactivateStartProcessButton() {
        startProcessButton.setEnabled(false);
    }

    public static void activateStartProcessButton() {
        startProcessButton.setEnabled(true);
    }



}