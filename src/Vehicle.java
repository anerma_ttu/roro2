public interface Vehicle {
    String getRegistrationNumber ();
    void setRegistrationNumber (String registrationNumber);
    VehicleType getVehicleType();
    float getVehicleLength ();
    float getVehicleWidth ();
    float getVehicleHeight();
    void  setXmax(float v);
    void  setXmin(float v);
    float getxMin();
    float getxMax();
    void getVehicleData ();
    float getVehicleMass();
    String getLaneString ();
    String getDeckString ();


}
