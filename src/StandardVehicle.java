import java.util.Random;

public class StandardVehicle implements Vehicle {
    VehicleType itsType;
    String registrationNumber;


    // constructors

    StandardVehicle() {

    }

    StandardVehicle(VehicleType itsType) {
        this.itsType = itsType;
    }


    StandardVehicle(VehicleType itsType, String registrationNumber) {
        this.itsType = itsType;
        this.registrationNumber = registrationNumber;
    }

    // end of constructors


    // Registration data functions
    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public static String registrationNumberGenerator() {
        Random rand = new Random();
        String numberString = String.valueOf(rand.nextInt(999));
        if (numberString.length() == 2) numberString = "0" + numberString;
        else if (numberString.length() == 1) numberString = "00" + numberString;
        else ;
        int leftLimit = 65; // letter 'A'
        int rightLimit = 90; // letter 'Z'
        int targetStringLength = 3;
        String letterString = rand.ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        String finalString = numberString + " " + letterString;
        return finalString;
    }


    @Override
    public String getRegistrationNumber() {

        return registrationNumber;
    }

    //
    @Override
    public VehicleType getVehicleType() {
        return itsType;
    }

    public float getVehicleLength() {
        return itsType.getVehicleLength();
    }

    @Override
    public float getVehicleWidth() {
        return itsType.getVehicleWidth();
    }

    @Override
    public float getVehicleHeight() {
        return itsType.getVehicleHeight();
    }

    @Override
    public void setXmax(float v) {
    }

    @Override
    public void setXmin(float v) {

    }

    @Override
    public float getxMin() {
        return 0;
    }

    @Override
    public float getxMax() {
        return 0;
    }

    public float getVehicleMass() {
        return itsType.getVehicleMass();
    }

    @Override
    public String getLaneString() {
        return null;
    }

    @Override
    public String getDeckString() {
        return null;
    }

    public void getVehicleData() {

        MainFrame.addData("\n Type: " + this.getVehicleType());
        MainFrame.addData("\n Reg number: " + this.getRegistrationNumber() + "\n");
    }


}
