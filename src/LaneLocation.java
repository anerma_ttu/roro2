public enum LaneLocation {
        FULL (true, false, false, false),
        BOW(false, true,false,false),
        STERN(false,false, false ,true),
        MIDDLE(false,false,true,  false);

        private boolean isBow;
        private boolean isMiddle;
        private boolean isStern;
        private boolean isFull;

        LaneLocation(boolean isFull, boolean isBow, boolean isMiddle, boolean isStern) {
            this.isFull = isFull;
            this.isBow = isBow;
            this.isMiddle = isMiddle;
            this.isStern = isStern;
        }
    }

